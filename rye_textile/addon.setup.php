<?php

return array(
  'author'      => 'The Rye Agency',
  'author_url'  => 'https://rye.agency',
  'name'        => 'Rye Textile',
  'description' => 'Outputs textile markup into HTML',
  'version'     => '1.0.0',
  'namespace'   => 'Rye\Textile'
);
