# Rye Textile

This add-on converts Textile markup to HTML, and optionally converts new lines to <br>.

## Requirements

The add-on has been tested with ExpressionEngine 3.4.1

## Installation

Copy the rye_textile file into your user add-ons file, typically found in `system/user/addons/`. Once copied over, install it from ExpressionEngine's add-on manager.

## Usage

### Standard
```
    {exp:rye_textile}
        {your_textile_field}
    {/exp:rye_textile}
```

### Convert new lines to `<br>` tags
```
    {exp:rye_textile nl2br="yes"}
        {your_textile_field}
    {/exp:rye_textile}
```

## Credit

This plugin uses the [PHP-Textile parser](https://github.com/textile/php-textile)
