<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'Textile/Parser.php';
require 'Textile/DataBag.php';
require 'Textile/Tag.php';

/**
 * Rye Textile
 *
 * @package     ExpressionEngine
 * @category    Plugin
 * @author      The Rye Agency
 * @copyright   Copyright (c) 2016, The Rye Agency
 * @link        https://rye.agency/
 */

class Rye_textile
{
    public $return_data = "";

    public function __construct()
    {
        $parser = new \Netcarver\Textile\Parser();

        // Grab params from template
        $nl2br = ee()->TMPL->fetch_param('nl2br');
        $markup = ee()->TMPL->tagdata;

        // Convert Textile > HTML
        if($nl2br=="yes"||$nl2br=="true"||$nl2br==1){
            $parsedText = nl2br($parser->parse($markup));
        } else {
            $parsedText = $parser->parse($markup);
        }

        $this->return_data = $parsedText;
    }

}
